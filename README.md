<img src="https://gitlab.com/sauce-api/core/-/raw/master/sauce-logo-earth-bg.png" alt="drawing" width="500px"/>

# Sauce API
Welcome to Sauce API

## Installing
```npm install -g @sauce-api/cli```

## Create Sauce Project
To create a new project just run ```sauce-api --name=<YOUR PROJECT NAME>```

## Sauce API Docs
Coming soon
