// Here you can put your custom route validator methods
// Example:
// 
// import { formatError } from "@sauce-api/core";
// import { Request } from "express";
// 
// export const isIsoDateParam = (requestParamValue :any, req :Request)=>{
//     const errorRes = formatError(400, "Date string must be in ISO8601:2000 format");
//     if (!/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/.test(requestParamValue)) {
//         throw errorRes;
//     };
//     const d = new Date(requestParamValue);
//     const isValid = Date && !isNaN(d as any) && d.toISOString()===requestParamValue; // valid date
//     if(!isValid) {
//         throw errorRes;
//     }
// };
