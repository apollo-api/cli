// This is where you can put common Route Params so you don't have to duplicate
// them across route definitions

// Example

// import { RouteParam } from "@sauce-api/core";

// export const UserId = new RouteParam()
//     .setName("userId")
//     .setType("string")
//     .setRequired(true)
//     .setDescription("The User's ID");
