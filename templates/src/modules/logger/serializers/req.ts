import { stdSerializers } from "bunyan";
import {Request} from "express";

const formatHeaders = headers =>{
    const {authorization, cookie, ...formattedHeaders} = headers; // eslint-disable-line no-unused-vars
    return formattedHeaders;
};

export function formatReq(req :Request) {
    const reqLogObj = stdSerializers.req(req);
    const {remoteAddress, remotePort, ...formattedReq} = reqLogObj; // eslint-disable-line no-unused-vars
    return {
        ...formattedReq,
        headers: formatHeaders(req.headers),
    };
};
