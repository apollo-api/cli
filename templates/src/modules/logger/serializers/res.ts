import {Response} from "express";

const formatHeaders = headers =>{
    const {Authorization, cookie, "set-cookie": setCookie, ...formattedHeaders} = headers; // eslint-disable-line no-unused-vars
    return formattedHeaders;
};

export function formatRes(res :Response) {
    return {
        statusCode: res.statusCode,
        headers: formatHeaders(res.getHeaders())
    };
};
