import {createLogger, LogLevelString, prototype as bunyan, stdSerializers, Stream} from "bunyan";
import * as PrettyStream from "bunyan-prettystream";
import { formatReq } from "./serializers/req";
import { formatRes } from "./serializers/res";
const prettyStdOut = new PrettyStream();

const nodeEnv = process.env.NODE_ENV || "development";

const streams :Stream[] = [];

// Setup pretty stream if running locally
if (nodeEnv == "development") {
    prettyStdOut.pipe(process.stdout);
    streams.push({
        type: "raw",
        stream: prettyStdOut,
        level: <LogLevelString>process.env.LOG_LEVEL || "debug",
    });
}
else{
    streams.push({
        stream: process.stdout,
        level: <any>process.env.LOG_LEVEL || "debug",
    });
}


const customizeBunyan = config => {
    if (nodeEnv !== "development") {
        config._emit = (rec, noemit) => {
            rec["message"] = rec.msg;
            delete rec.hostname;
            delete rec.pid;
            delete rec.msg;
            delete rec.v;

            (<any>bunyan)._emit.call(config, rec, noemit);
        };
    }
    return config;
};

const bunyanConfig = createLogger({
    name: "sauce-api",
    streams,
    serializers: {
        req: formatReq,
        res: formatRes,
        err: stdSerializers.err,
        guid: guid =>{
            return guid;
        },
        tag: tag =>{
            return tag;
        },
    },
});

export const BunyanLogger = customizeBunyan(bunyanConfig);
