import { LogLevelString } from "bunyan";
import { BunyanLogger } from "./bunyanConfig";

const tag = `sauce-api.${process.env.ENV || "local"}`;

function formatLogData(arg, str) {
    let serializerObj = {
        tag,
    };
    let logMessage = "";

    if (!arg && str && typeof str == "string") {
        logMessage = str;
    }
    else if (typeof(arg) == "string") {
        logMessage = arg || str;
    }
    else if (typeof(arg) == "object") {
        arg = {
            ...arg,
            tag,
            guid: getGuidFromHeaders(arg),
        };
        serializerObj = arg;
        logMessage = str;
    }

    return [serializerObj, logMessage];
}

function getGuidFromHeaders(arg) {
    const headers = arg?.req?.headers || arg?.res?.getHeaders();
    let guid;
    if (headers && headers["x-request-guid"]) {
        guid = headers["x-request-guid"];
    }
    return guid;
}

function applyToLog(method :LogLevelString, str, arg, ...extraArgs) {
    if (!(process.env.NODE_ENV === "test" && process.env.RUN_TESTS_WITH_LOGS == "false")) {
        const args = formatLogData(arg || arg, str);
        if (args) {
            BunyanLogger[method].apply(BunyanLogger, [...args, ...extraArgs]);
        }
        else {
            BunyanLogger[method].apply(BunyanLogger, [str, ...extraArgs]);
        }
    }
}


export const info = (str?:any, arg?:any, ...extraArgs)=>{
    applyToLog("info", str, arg, ...extraArgs);
};

export const error = (str?:any, arg?:any, ...extraArgs)=>{
    applyToLog("error", str, arg, ...extraArgs);
};

export const debug = (str?:any, arg?:any, ...extraArgs)=>{
    applyToLog("debug", str, arg, ...extraArgs);
};
