declare global {
    // eslint-disable-next-line no-unused-vars
    namespace NodeJS {
        // eslint-disable-next-line no-unused-vars
        interface ProcessEnv {
            // You can add any env vars here for typescript support
            NODE_ENV :"development" | "production" | "test"
        }
    }
}

export {};
