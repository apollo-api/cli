// This is where you'd put your route policies
// See https://sauce-api.gitlab.io/docs/policies


import { formatError } from "@sauce-api/core";
import { SauceType } from "./SauceConfig";
export interface Policies{
    // isAuthenticated: (Sauce :SauceType) => Promise<void>
}
export type PolicyNames = keyof Policies;

export const PolicyMethods :Policies = {
    // isAuthenticated: async (Sauce :SauceType) :Promise<void> =>{
    //     try{
    //         const token = Sauce.req.get("Authorization");
    //         if(token) {
    //             const validToken = await validateToken(token);
    //             if(!validToken) {
    //                 throw formatError(400, "Invalid Token");
    //             }
    //         }
    //         else{
    //             throw formatError(401, "You're not authorized to make this request");
    //         }
    //     }
    //     catch(err) {
    //         throw err;
    //     }
    // },
};
