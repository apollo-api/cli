import { Sauce, SauceConfig } from "@sauce-api/core";
import * as path from "path";
import { RouteDefinitions } from "../../api/RouteDefinitions";
import * as logger from "../../modules/logger/logger";
import { Policies, PolicyMethods } from "./Policies";
import PaginationConfig from "./PaginationConfig";

const isProd = process.env.NODE_ENV === "production";

export type SauceCustom = {
    // Anything you want to have in your
    // Sauce Custom object (see: https://sauce-api.gitlab.io/docs/sauce-object)
    // You should put here for type enforcement
}
export type SauceType = Sauce<SauceCustom>;

export const sauceConfig :SauceConfig<SauceCustom, Policies> = {
    routes: RouteDefinitions,
    controllerDirectory: path.resolve(__dirname, "../../api"),
    controllerExtension: isProd ? "js" : "ts",
    policies: PolicyMethods,
    environments: {
        local: {
            url: "http://localhost:1337"
        },
        qa: {
            url: "http://some-qa-site"
        },
        prod: {
            url: "https://some-production-site"
        }
    },
    logger,
    pagination: PaginationConfig
};
