import { Route } from "@sauce-api/core";

const root :Route = new Route()
    .setMethod("GET")
    .setPath("/")
    .setController("root")
    .setAction("index")
    .setDescription("Check to see if the API is running");

export default [
    root
];
