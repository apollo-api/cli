import { Controller } from "@sauce-api/core";

export class RootController extends Controller {
    public index() :any {
        return this.responses.responseText(200, "Healthy");
    }
}
