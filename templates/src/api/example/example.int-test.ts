import * as request from "supertest";
import {expect} from "chai";
import {app} from "../../test-utils/testApp";

describe("Example", ()=> {
    describe("GET /example", async ()=> {
        it("Should return example data as expected", async ()=>{
            await request(app.getApp())
                .get("/example")
                .expect(200)
                .expect(res => {
                    expect(res.body.page).to.be.an("object");
                    expect(res.body.response).to.deep.equal([
                        {
                            id: 1,
                            firstName: "Bruce",
                            lastName: "Wayne",
                            age: 26
                        },
                        {
                            id: 2,
                            firstName: "Clark",
                            lastName: "Kent",
                            age: 30
                        }
                    ]);
                });
        });
        
        it("Should return filtered example data as expected", async ()=>{
            await request(app.getApp())
                .get("/example?firstName=clark")
                .expect(200)
                .expect(res => {
                    expect(res.body).to.be.an("object");
                    expect(res.body.page).to.be.an("object");
                    expect(res.body.response).to.deep.equal([
                        {
                            id: 2,
                            firstName: "Clark",
                            lastName: "Kent",
                            age: 30
                        }
                    ]);
                });
        });
    });
    
    describe("GET /example/:userId", async ()=> {        
        it("Should return singular example data", async ()=>{
            await request(app.getApp())
                .get("/example/2")
                .expect(200)
                .expect(res => {
                    expect(res.body.response).to.deep.equal({
                        id: 2,
                        firstName: "Clark",
                        lastName: "Kent",
                        age: 30
                    });
                });
        });
        
        it("Should return singular example data", async ()=>{
            await request(app.getApp())
                .get("/example/1000001")
                .expect(404)
                .expect(res => {
                    expect(res.body.error_description).to.deep.equal("User 1000001 does not exist");
                });
        });
    });
});
