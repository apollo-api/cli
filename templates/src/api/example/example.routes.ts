import { getPaginationParams, Route, RouteParam } from "@sauce-api/core";
import PaginationConfig from "../../config/Sauce/PaginationConfig";

const getUsers :Route = new Route()
    .setMethod("GET")
    .setPath("/example")
    .setController("example")
    .setAction("getUsers")
    .setDescription("Get example users")
    .setQueryParams([
        ...getPaginationParams(PaginationConfig.max),

        new RouteParam()
        .setName("firstName")
        .setType("string")
        .setDescription("Filter by firstName")
        .setRequired(false),

        new RouteParam()
        .setName("age")
        .setType("number")
        .setDescription("Filter by age")
        .setRequired(false)
    ]);

const getSingleUser :Route = new Route()
    .setMethod("GET")
    .setPath("/example/:userId")
    .setController("example")
    .setAction("getSingleUser")
    .setDescription("Get a single example user")
    .setPathParams([
        new RouteParam()
        .setName("userId")
        .setType("number")
        .setDescription("Get a specific user")
        .setRequired(true)
    ]);

export const ExampleRoutes :Route[] = [
    getUsers,
    getSingleUser
];
