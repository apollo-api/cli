import { Controller, formatError } from "@sauce-api/core";
import { SauceType } from "../../config/Sauce/SauceConfig";
import {ExampleService} from "./example.service";

export class VersionController extends Controller {
    private service :ExampleService;

    constructor(Sauce :SauceType) {
        super(Sauce);
        this.service = new ExampleService(Sauce);
    }

    public async getUsers() :Promise<void> {
        try{
            const firstName :string = <any>this.req.query["firstName"];
            
            // Age is automatically parsed as a number!
            const age :number = <any>this.req.query["age"];

            const users = await this.service.getUsers({firstName, age});
            return this.responses.responsePaginated(200, this.service.paginate(users));
        } catch(err){
            this.next(err);
        }
    }

    public async getSingleUser() :Promise<void> {
        try{
            // userId is automatically parsed as a number!
            const id :number = <any>this.req.params["userId"];
            const user = await this.service.getUser(id);
            if(!user){
                // Easily send errors to the global error handler
                throw formatError(404, `User ${id} does not exist`);
            }
            return this.responses.responseObject(200, user);
        } catch(err){
            this.next(err);
        }
    }
}
