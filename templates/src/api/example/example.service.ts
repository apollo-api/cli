import { Service } from "@sauce-api/core";
import { SauceType } from "../../config/Sauce/SauceConfig";

interface ExampleUser {
    id :number
    firstName :string
    lastName :string
    age :number
}
interface IUsersFilter {
    firstName ?:string
    age ?:number
}

export class ExampleService extends Service {
    constructor(Sauce :SauceType) {
        super(Sauce);
    }

    private users :ExampleUser[] = [
        {
            id: 1,
            firstName: "Bruce",
            lastName: "Wayne",
            age: 26
        },
        {
            id: 2,
            firstName: "Clark",
            lastName: "Kent",
            age: 30
        },
    ];

    /**
     * Typically you'd get this data from a DB or service of some sort. This method is just to simulate
     * the fetching of data, hence the promise.
     */
    public async getUsers({firstName, age} :IUsersFilter = {}) :Promise<ExampleUser[]> {
        try {
            if(firstName) {
                return this.users.filter(user => user.firstName.toLowerCase() == firstName.toLowerCase())
            }
            if(age) {
                return this.users.filter(user => user.age == age)
            }
            return this.users;
        }
        catch(err){
            throw err;
        }
    }

    public async getUser(id :number) :Promise<ExampleUser> {
        try {
            const result = this.users.filter(user => user.id == id);
            return result.length ? result[0] : null;
        }
        catch(err){
            throw err;
        }
    }
}
