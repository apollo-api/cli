import { Route } from "@sauce-api/core";
import { ExampleRoutes } from "./example/example.routes";
import rootRoutes from "./root/root.routes";

export const RouteDefinitions :Route[] = [
    ...ExampleRoutes,
    ...rootRoutes,
];
