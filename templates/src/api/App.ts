import * as express from "express";
import { Application } from "express";
import * as onFinished from "on-finished";
import {green} from "chalk";
import {info, debug} from "../modules/logger/logger";
import { ErrorHandler } from "../config/ErrorHandler";
import * as cors from "cors";
import { visualizerUI, setupVisualizerUI } from "@sauce-api/visualizer-ui";
import { Responses, Rocket, Routes, } from "@sauce-api/core";
import { sauceConfig, SauceCustom } from "../config/Sauce/SauceConfig";
import { v4 as uuidv4 } from "uuid";

/**
 * Example Express application that's wired up with Sauce
*/
export class App {
    public app :Application;
    public port :number = 1337;

    constructor() {
        info("Getting the Sauce...");
        this.app = express();
    }

    public async init() {
        this.enableCors();
        this.registerDocsUi();
        this.setupBodyParsers();
        this.setupReqLogger();
        this.bindRoutes();
        this.setupErrorHandler();
    }

    private setupReqLogger() :void {
        if(process.env.NODE_ENV !== "test"){
            this.app.use((req, res, next)=>{
                req.headers["x-request-guid"] = uuidv4();
                info("Logging request", {req});
    
                res.set("x-request-guid", req.headers["x-request-guid"]);
                onFinished(res, (err, res)=>{
                    info("Sending response", {res});
                });
                next();
            });
        }
    }

    private enableCors() :void {
        this.app.use(cors());
    }

    /**
     * You can access the Visualizer UI at /docs
     *
     * Visualizer UI: https://www.npmjs.com/package/@sauce-api/visualizer-ui
    */
    private registerDocsUi() :void {
        debug("Setting up docs UI at /docs");
        setupVisualizerUI(this.app);
        this.app.get("/docs", async (req, res, next)=>{
            const appRoutes = await new Routes(sauceConfig).getFormattedRoutes(true);
            const tpl = await visualizerUI({
                appRoutes,
                defaultTheme: "night",
                appName: "GuestSnaps API",
                brandIcon: "camera"
            });
            res.send(tpl);
        });
    }

    private setupBodyParsers() :void {
        this.app.use(<any>express.json());
        this.app.use(<any>express.urlencoded({extended: true}));
    }

    private setupErrorHandler() :void {
        this.app.use((err, req, res, next) => {
            new ErrorHandler(res, err).handleError();
        });
    }

    private bindRoutes() :void {
        try{
            debug("Binding Routes...");

            new Routes(sauceConfig).bindRoutes<SauceCustom>({
                app: this.app,
                sauceCustom: {
                    // Anything you want to have in your
                    // Sauce Custom object (see: https://sauce-api.gitlab.io/docs/sauce-object)
                    // You should put in here.
                    // Don't forget to add it in your SauceConfig as well
                },
            });

            this.app.get("*", (req, res)=>{
                this.notFound(req, res, "GET");
            });
            this.app.post("*", (req, res)=>{
                this.notFound(req, res, "POST");
            });
            this.app.put("*", (req, res)=>{
                this.notFound(req, res, "PUT");
            });
            this.app.delete("*", (req, res)=>{
                this.notFound(req, res, "DELETE");
            });

            debug("Routes bound.");
        }
        catch(err) {
            console.error(err);
            throw err;
        }
    }

    private notFound(req, res, method) :void {
        new Responses(res).notFound(`${method} ${req.path} is not a valid operation`);
    }

    private setupHttpServer() :void {
        this.app.listen(this.port, () => {
            this.displayRocket();
            info(green(`Sauce API has launched on port ${this.port}!`));
        });
    }

    private displayRocket() {
        if(process.env.NODE_ENV == "development") {
            new Rocket(sauceConfig).launch();
        }
    }

    public async listen() {
        this.setupHttpServer();
    }

    public getApp() :Application {
        return this.app;
    }
}
