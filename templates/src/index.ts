import {App} from "./api/App";
import { error } from "./modules/logger/logger";

const app :App = new App();
app.init()
    .then(()=>app.listen())
    .catch((err)=>error("FAILED BUILDING APPLICATION", err));
