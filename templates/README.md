# Sauce API
Welcome to Sauce API

## Starting things up
You can launch this app with ```npm run start-dev```

It will startup on port `1337` by default

## Running tests
`npm run test`