#!/usr/bin/env node
import fs from 'fs-extra';
import {dirname, join} from "path";
import yargs from 'yargs/yargs';
import { hideBin } from 'yargs/helpers';
import chalk from 'chalk';
import { fileURLToPath } from 'url';
const { name } = yargs(hideBin(process.argv)).argv
const __dirname = dirname(fileURLToPath(import.meta.url));

try {
    if(!name){
        console.error(chalk.red("Please provide a name using the --name argument"));
        process.exit(1);
    }
    
    console.log(chalk.cyan(`Building project: ${name}...`));
    const destination = join(process.cwd(), `${name}`);
    fs.copySync(join(__dirname, '../templates'), destination, { overwrite: false });
    console.log(chalk.green("Successfully finished building the project!"));
} catch (err) {
    console.error(err);
}
