const {exec} = require('child_process');

const execPromise = (command)=>{
    return new Promise(function(resolve, reject) {
        exec(command, (error, stdout, stderr) => {
            if (error) {
                reject(error);
                return;
            }
            resolve(stdout.trim());
        }).stdout.pipe(process.stdout);
    });
}
